static int PORT;
static int MAX_CLIENTS;
static int *cli_counter;	// 

void handle_client(int sd)
{
	(*cli_counter)++;
	printf("cli_counter = %d\n", *cli_counter);
	fd_set read_sd;
	FD_ZERO(&read_sd);
	FD_SET(sd, &read_sd);
	while (1) 
	{
		fd_set rsd = read_sd;

		int sel = select(sd + 1, &rsd, 0, 0, 0);

		if (sel > 0) 
		{

			char buf[1024] = {0};
			
			int bytes = recv(sd, buf, sizeof(buf), 0);
			if (bytes > 0) 
			{
				// got data from the client.
                // test
				printf("> %s", buf);
				send(sd, buf, strlen(buf), 0);

			}
			else if (bytes == 0) 
			{
				printf("client disconnected\n");
				break;
			}
			else 
			{
				// error receiving data from client
                // todo
			}
		}
		else if (sel < 0) 
		{
			// fatal error occurred.
            // todo
		}
	}
	close(sd);
}

void read_from_file()
{
	FILE * fp;
	char * line = NULL;
	char * port;
	char * clients; 
	size_t len = 0;
	ssize_t data;
	fp = fopen("./config.txt", "r");
    if (fp == NULL)
		exit(EXIT_FAILURE);

	while ((data = getline(&line, &len, fp)) != -1) 
	{
		if ( strncmp(line, "PORT =", 5) == 0 )
		{
			port = line + 6;
			port[strlen(port - 1)] = '\0';
            PORT = atoi(port);
		}
		if ( strncmp(line, "MAX_CLIENTS =", 13) == 0 ) 
		{
			clients = line + 13;
			clients[strlen(clients - 1)] = '\0';
		    MAX_CLIENTS = atoi(clients);
        }
	}

	fclose(fp);
	if (line)
		free(line);

}


void my_error(char * to_print)
{
	fprintf(stderr, "%s", to_print);
	exit(1);
}
