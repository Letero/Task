#define _GNU_SOURCE
#include <stdio.h>	
#include <stdlib.h>	
#include <unistd.h>	
#include <netdb.h>
#include <netinet/in.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>
#include "server.h"


int main(int argc, char *argv[]) 
{
	read_from_file();

	/*using shared memory to retrieve information from child processes (clients)*/
	cli_counter = mmap(NULL, sizeof *cli_counter, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	*cli_counter = 0;
	int sockfd, newsockfd;
	unsigned int cli_len;
	struct sockaddr_in serv_addr, cli_addr;
	int pid; 

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0) 
	{
		my_error("ERROR opening socket");
	}

	bzero((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htons(INADDR_ANY);
	serv_addr.sin_port = htons(PORT);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
	{
		my_error("ERROR on binding");
	}

	listen(sockfd,5);
	cli_len = sizeof(cli_addr);

	while (1) 
	{

		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &cli_len);

		if (newsockfd < 0) 
		{
			my_error("ERROR on accept");
		}
		if(*cli_counter >= MAX_CLIENTS)
		{
			printf("Server is at it's max capacity. Try to connect later\n");
		}
		else 
		{
			pid = fork();
			if (pid < 0) 
			{
				my_error("ERROR on fork");
			}

			if (pid == 0) 
			{ /*client process*/
				close(sockfd);
				handle_client(newsockfd);
				(*cli_counter)--;
				exit(0);

			}
		}
		close(newsockfd);

	} 
	munmap(cli_counter, sizeof *cli_counter);
}

